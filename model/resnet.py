r"""
    CIFAR pretrained ResNet sample
    Author: Mr. Balsdnim
    Date:   1st Dec. 2021
"""

import numpy as np
import torch


class CifarExampleModel:
    def __init__(self):
        """TODO: Initialize classification service with the given torch model.
        """

if __name__ == "__main__":
    # model load
    model = torch.hub.load("chenyaofo/pytorch-cifar-models", "cifar10_resnet20", pretrained=True)

    # data preprocess
    image = np.random.randint(256, size=(1, 3, 32, 32))
    image_tensor = torch.tensor(image / 255.0)
    image_tensor = image_tensor.float()

    # inference
    predictions = model(image_tensor)  # [1, 10]
    prediction = predictions[0]  # [10]
    prediction = prediction.argmax()  # [1] prediction result
    print(int(prediction))
