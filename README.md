# MINDsLab Inc. ML Engineer Coding Test
* Authors: MINDsLab Brain
* Date: 1st Dec. 2021

Skeleton codes for `Classification service implementation via gRPC protocol` problem.
This problem does not need CUDA-enabled GPU devices.

You can import any library if you need.  
But if you import other libraries, please make sure to add the list in the [`requirements.txt`](./requirements.txt).

## Tips

### proto build
To build proto, you can type the command in the below.

```bash
python -m grpc.tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ classification.proto
```

Then, you will get [`classification_pb2_grpc.py`](./classification_pb2_grpc.py) and [`classification_pb2.py`](./classification_pb2.py), which should not be modified.

### PyTorch model selection

As mentioned in [`model/resnet.py`](./model/resnet.py), you can check the example model from [`torch.hub`](https://pytorch.org/hub/).

```python
model = torch.hub.load("chenyaofo/pytorch-cifar-models", "cifar10_resnet20", pretrained=True)
```

You can use the given pretrained model for your service.  
Note that we do not care about the accuracy of service.


## Usage
1. Install [`requirements.txt`](./requirements.txt)
Please type the following command to install necessary libraries.

```bash
pip install -r requirements.txt
```

2. Check if gRPC server and client work

An example service is built for text input. You can simply test whether gRPC service is working well with the following command.

```bash
# Server-side
python server.py

# Client-side
python client.py
```

3. Solve the Mr. Balsdnim's problem

Please solve problems given in the `.pdf` file.


## Reference

- https://github.com/chenyaofo/pytorch-cifar-models

Copyright MINDsLab Inc. 2021
