r"""
    Classification server sample
    Author: Mr. Balsdnim
    Date:   1st Dec. 2021
"""
from concurrent import futures
import argparse
import os
import sys
import time
import cv2
import torch
import torch.nn.functional as F
import numpy as np
import logging
from model.resnet import CifarExampleModel

import grpc

from classification_pb2_grpc import ClassificationServicer, add_ClassificationServicer_to_server
from classification_pb2 import SampleOutput, Output, AIRPLANE, AUTOMOBILE, BIRD, CAT, DEER, DOG, FROG, HORSE, SHIP, TRUCK

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_CLASS_DICT = {
    0: AIRPLANE,
    1: AUTOMOBILE,
    2: BIRD,
    3: CAT,
    4: DEER,
    5: DOG,
    6: FROG,
    7: HORSE,
    8: SHIP,
    9: TRUCK
}


def get_encoded_text(chunks):
    result = bytearray()
    for chunk in chunks:
        result.extend(chunk.text)
    return bytes(result)


class ClassificationServer(ClassificationServicer):
    def __init__(self):
        self.model = CifarExampleModel()

    def GetTextResult(self, request, context):
        """Example stub definition for text input

        Args:
            request: gRPC chunk or gRPC chunk iterator
            context: gRPC context

        Returns:
            gRPC chunk: chunk output with int result
        """
        binary = get_encoded_text(request)
        text = binary.decode('utf-8')

        logging.info(f"Request Text: {text}")

        try:
            length = len(text)
            return SampleOutput(result=length)
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.ABORTED)
            context.set_details(str(e))

    def GetResult(self, request, context):
        """TODO: skeletal code for image classification service

        Args:
            request: gRPC chunk or gRPC chunk iterator
            context: gRPC context

        Returns:
            gRPC chunk: chunk output with class index result
        """
        raise NotImplementedError


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=str, default="6010",
                        help='Remote IP and Port')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    logging.getLogger().setLevel(logging.INFO)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    service_server = ClassificationServer()
    servicer = add_ClassificationServicer_to_server(service_server, server)
    server.add_insecure_port(f'[::]:{args.port}')
    server.start()

    print(f"Classification service starts... | PORT: {args.port}")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
