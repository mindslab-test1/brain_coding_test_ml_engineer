r"""
    Classification client sample
    Author: Mr. Balsdnim
    Date:   1st Dec. 2021
"""
import grpc
import argparse
import os
import sys

from classification_pb2_grpc import ClassificationStub
from classification_pb2 import Input, SampleInput

TEXT_CHUNK_SIZE = 8
CHUNK_SIZE = 64  # bytes

_CLASS_DICT = {
    0: "AIRPLANE",
    1: "AUTOMOBILE",
    2: "BIRD",
    3: "CAT",
    4: "DEER",
    5: "DOG",
    6: "FROG",
    7: "HORSE",
    8: "SHIP",
    9: "TRUCK"
}


def generate_text_iterator(text):
    for idx in range(0, len(text), TEXT_CHUNK_SIZE):
        yield SampleInput(text=text[idx:idx + TEXT_CHUNK_SIZE])


class ClassificationClient:
    def __init__(self, remote):
        self.channel = grpc.insecure_channel(remote)
        self.stub = ClassificationStub(self.channel)

    def get_text_result(self, text):
        """Example client function for text input

        Args:
            text (str): input text

        Returns:
            int: result value (in this case, text length)
        """
        bytes_text = bytes(text, encoding="UTF-8")
        binary_iterator = generate_text_iterator(bytes_text)
        result = self.stub.GetTextResult(binary_iterator)
        return result.result

    def get_result(self, image_path):
        """TODO: skeletal function for stub calling

        Args:
            image_path (str): image path
            
        Returns:
            str: predicted class
        """
        raise NotImplementedError


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--remote', type=str, default="127.0.0.1:6010",
                        help='Remote IP and Port')
    parser.add_argument('--text', type=str, default="Hello, This is MINDsLab!",
                        help='Input text')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    client = ClassificationClient(args.remote)

    result = client.get_text_result(args.text)
    # result = client.get_result(image_path)
    print(result)
